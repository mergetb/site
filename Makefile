all: \
	build/commander \
	build/cmdr

pbs=api/shared.pb.go \
		api/driver.pb.go \
		api/commander.pb.go \

libcmdr_src=pkg/config.go \
						pkg/logging.go \
						pkg/types.go \
						pkg/etcd.go \
						pkg/util.go \
						pkg/store.go \
						pkg/health.go \
						pkg/vars.go \
						pkg/register.go \
						pkg/diags.go

prefix ?= /usr/local

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

.tools:
	$(QUIET) mkdir .tools

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/site/pkg.Version=$(VERSION)"

# commander ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

build/commander: commander/main.go $(pbs) $(libcmdr_src) | build
	$(go-build)

build/cmdr: util/cmdr/main.go $(pbs) $(libcmdr_src) | build
	$(go-build)

$(pbs): api/commander.proto api/driver.proto api/shared.proto $(protoc-gen-go)
	$(protoc-build)
	$(QUIET) sed -i 's/`json:"-"`/`json:"-" yaml:"-"`/g' $@

build/mock-commander: testing/mock-commander/main.go $(pbs) | build
	$(go-build)

build/mock-driver: testing/mock-driver/main.go $(pbs) | build
	$(go-build)

build:
	$(QUIET) mkdir build

clean:
	$(QUIET) rm -rf build
	$(QUIET) find ./commander -name *.pb.go | xargs -n 1 rm -f

distclean: clean
	$(QUIET) rm -rf vendor
	$(QUIET) rm -rf Gopkg.lock

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

.PHONY: install
install: build/commander
	install -D build/commander $(DESTDIR)$(prefix)/bin/commander

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $(dir $<)*.go
endef

define go-build-file
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $<
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I ./api \
		-I ./$(dir $@) \
		$(dir $<)*.proto \
		--go_out=plugins=grpc:./api
endef
