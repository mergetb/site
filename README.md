# MergeTB site

This repository holds the implementation of the common elements deployed by all ceftb sites, plus a few drivers that may be commonly useful across sites.

<!-- ![comms](docs/img/comms.png) -->
<p align="center"><img width="500" src="docs/img/comms.png"></img></p>

## Commander

This component is responsible for all communications with the portal. When the portal sends commands to a site, they go through the commander. 
The commander is also responsible for managing the drivers at a site. So in essence, the commander exposes two APIs 

- one for the portal to control drivers
- one for drivers to report progress on materializations to the portal

Each of these interfaces is a gRPC (protobuf 3) service definition.

## Drivers

Each driver is associated to a device type. For example there are drivers for Android and Raspberry Pi devices. Each instance of a driver type may control multiple devices. The driver model in Merge is very similar to the operating system driver model. Drivers expose a common interface for the commander. This common interface is based on a simple state machine model that is designed to be applicable to any device, see the diagram below. Clearly any non-trivial device has many special features an idiosyncrasies that must be accommodated by a driver if it is to be useful. These complexities must be captured in the model data that is passed to the device through the minimal set of API that comprise the driver API. This way the device drivers may evolve in any way they see fit as well as accommodate arbitrarily complex scenarios while still maintaining a common stable interface. Drivers can be written in any language, they just need to support the interface definition (the easiest path to doing so is through gRPC).

<p align="center"><img width="500" src="docs/img/device-sm.png"></img></p>


## Building

```
make
```

