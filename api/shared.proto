syntax = "proto3";
package site;

// top level messages used by other messages
message Diagnostic {
  // use standard 5 level code
  enum Code { 
    Trace = 0;
    Debug = 1;
    Info = 2;
    Warn = 3;
    Error = 4;
    Critical = 5;
  }
  Code code = 1;
  string msg = 2;
  map<string, string> fields = 3;
}

///
/// Payload wrappers
///

message InterfaceModel {
  string vlid = 1;   // virtual link id
  string phyid = 3;  // physical interface id
  string model = 2;  // json
}

message NodeModel {
  string model = 1; // json
  repeated InterfaceModel interfaces = 2;
}

message LinkModel {

  // virtual link id
  string vlid = 1;

  // How many experiment lanes each physical testbed network segment carries.
  // Maps physical testbed link IDs to a number of lanes. A LinkModel is the
  // mapping of an experiment link over a set of testbed links. For any given
  // nodes X and Y in a testbed, it will generally require several hops (across
  // switches and the like) to get from X to Y. A Lane indicates residency of a
  // unique LinkModel on a particular testbed link segment. Thus, while a single
  // LinkModel will not impart multiple lanes across testbed link segments,
  // LinkModels exist as a part of a broader experiment and can create multiple
  // experiment lanes of traffic running over a testbed link segments. This map
  // captures that multi-tenancy so that the provisioning systems responsible
  // for implementing this link model are aware of the lane configuration and
  // and setup the corresponding virtual link appropriately.
  map<string, uint32> lanes = 2;

  // json
  string model = 3;

  // endpoints that are a part of this link
  repeated string endpoints = 4;

}

message AssetModel {
  string model = 1; // json of the asset object
}

message AssetFragment {
  string id = 1;
  repeated string resources = 2; // which driver handles
  AssetModel model = 3; // used for static storage

  enum State {
    Reserved = 0;
    Pending = 1;
    Reticulating = 2;
    Complete = 3;
    Error = 4;
  }

  State state = 4;
  string error = 5;
}

message AssetRequest {
  string assetid = 1;
  repeated AssetFragment fragments = 2;
}

message AssetResponse {
  string assetid = 1;
  repeated Diagnostic msg = 2;
  repeated AssetFragment updates = 3;
}

message MzFragment {
  string id = 1;
  repeated string elements = 2;
  repeated string resources = 3;
  oneof model {
    NodeModel node_model = 4;
    LinkModel link_model = 5;
    AssetModel asset_model = 9; // used for experiment storage
  }
  string allocation = 6;

  enum State {
    Reserved = 0;
    Pending = 1;
    Reticulating = 2;
    Complete = 3;
    Error = 4;
  }
  State state = 7;
  string error = 8;
}

message MzRequest {
  string mzid = 1;
  string instance = 2;
  repeated MzFragment fragments = 3;
}

message OpResult {
  string resourceId = 1;
  repeated Diagnostic msg = 2;
  repeated MzFragment updates = 3;
}

message MzResponse {
  string mzid = 1;
  string instance = 2;
  repeated OpResult results = 3;
}


message HealthRequest {}
message HealthResponse {
  int32 drivers = 1;
  int32 healthy_drivers = 2;
}

message IncomingRequest {
  string mzid = 1;
  string instance = 2;
  enum Code {
    Reserved = 0;
    Setup = 1;
    Teardown = 2;
  }
  Code code = 3;

  // user provided network properties
  string data = 4;
}

message IncomingResponse {}

message StatusRequest {
  string mzid = 1;
  string instance = 2;
}

message StatusResponse {
  repeated Task tasks = 1;
}

message Task {
  string kind = 1;
  string name = 2;
  string error = 3;
  bool complete = 4;
}
