#!/bin/bash

set -e

docker build -f debian/builder.dock -t site-builder .
docker run -v `pwd`:/site site-builder /site/build-deb.sh

