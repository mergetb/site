#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/mergetb-commander*.build*
rm -f build/mergetb-commander*.change
rm -f build/mergetb-commander*.deb

debuild -e V=1 -e prefix=/usr $DEBUILD_ARGS -i -us -uc -b

mv ../mergetb-commander*.build* build/
mv ../mergetb-commander*.changes build/
mv ../mergetb-commander*.deb build/
