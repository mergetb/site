# Commander

The commander is responsible for communicating with

- the portal
- drivers
- side administrators
- hummingbird
- local ceph node

All of these communications are implemented using [gRPC](https://grpc.io). This document describes the communication patterns involved for each participant.

## Portal

The commander communicates with the portal for three primary reasons, 

- `register`/`unregister` the site
- `commission`/`decommission` resources
- `provision`/`deprovision` resources

Each of these is described in detail below.

### Registration

The registration process brings a new site into the ceftb ecosystem. This process is secured by a TLS authentication flow that looks like the following.

![reg](../docs/img/reg.png)

In order for the registration process to work, one of two things needs to be true

- The commander is using a valid TLS cert from a legit certificate authority like [Let's Encrypt](https://letsencrypt.org).
- The commander is using a TLS cert signed by the ceftb certificate authority (for use in self-provider cases only).

### Data Model

```
/driver/$driver/host --> $host
/driver/$driver/port --> $port
```

## Driver

The drivers interface with the portal through the site commander.  The commander exposes all of the functions to implement the driver state machine by which the portal utilizes.  It also exposes:

- `register`/`unregister` drivers to site

Register and unregister allow a site to keep an accurate account of the available devices currently being controlled through the driver.

### Development

The commander dependencies are now managed using [dep](https://golang.github.io/dep/). To install 

```
go get -u github.com/golang/dep/cmd/dep
```

To grab all the dependencies, from this directory execute
```
dep ensure
```

[https://golang.github.io/dep/docs/installation.html](https://golang.github.io/dep/docs/installation.html)

#### Specific Details

First make sure you have variables for GOPATH and GOROOT.  These will generally point to $HOME/go as per the go documentation ([go command](https://golang.org/doc/articles/go_command.html)):

```
As mentioned above, the default $GOPATH on Unix is $HOME/go
```

If a src directory does not exist within $GOPATH, create the following path:

`mkdir -p $GOPATH/src/gitlab.com/mergetb`

Then you will need to create a symlink from within the ceftb directory to where you git cloned the site repository.  If for example we `git clone` site to our home directory:

`ln -s $HOME/site/ $GOPATH/src/gitlab.com/mergetb/site`

This is a requirement of dep ([dep new project](https://golang.github.io/dep/docs/new-project.html)), that you must have the dep path rooted in $GOPATH.  You can now use the symlink to:

`cd $GOPATH/src/gitlab.com/mergetb/site && dep ensure`

If the path above is not correct, but site has been symlinked within the site repository, you may see that when you run `dep status`:

```
PROJECT                     CONSTRAINT     VERSION        REVISION  LATEST   PKGS USED
gitlab.com/mergetb/site       branch master  branch master  d60397b   81e4ea5  1
```

This means that the path has not been set currently, you should not see this in the output. Verify that there are no spelling errors on the path to the site repo. The correct response is shown below:

```
PROJECT                     CONSTRAINT     VERSION        REVISION  LATEST   PKGS USED
github.com/golang/protobuf  v1.0.0         v1.0.0         9255415   v1.0.0   5
github.com/sirupsen/logrus  ^1.0.4         v1.0.4         d682213   v1.0.4   1
golang.org/x/crypto         branch master  branch master  91a49db   91a49db  1
golang.org/x/net            branch master  branch master  22ae77b   22ae77b  7
golang.org/x/sys            branch master  branch master  f6cff07   dd2ff4a  2
golang.org/x/text           v0.3.0         v0.3.0         f21a4df   v0.3.0   14
google.golang.org/genproto  branch master  branch master  2c5e7ac   2c5e7ac  1
google.golang.org/grpc      ^1.10.0        v1.10.0        8e4536a   v1.10.0  23
```

After dep ensure has been run, the next step is to run `make`, make will build all the relevent code and go binaries.  The last step is to run `make containers` this will create a docker container commander, that you can run to test your commander code.

You should now be able to run: `docker run commander`
