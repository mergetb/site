package main

//TODO: just like logs are better when structured, diagnostics are better when
//      structured too

import (
	"context"
	"flag"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	api "gitlab.com/mergetb/site/api"
	site "gitlab.com/mergetb/site/pkg"
)

var (
	// CmdrHost hostname of commander
	CmdrHost = flag.String("listen", "0.0.0.0", "listen address")
	// CmdrPort port number of commander
	CmdrPort = flag.Int("port", 6000, "listen port")
	Cert     = flag.String("cert", "/etc/merge/cmdr.pem", "TLS API cert")
	Key      = flag.String("key", "/etc/merge/cmdr-key.pem", "TLS API cert")

	maxMessageSize    = 512 * 1024 * 1024
	GRPCServerOptions = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(maxMessageSize),
		grpc.MaxSendMsgSize(maxMessageSize),
	}
)

type srv struct{}

func (s *srv) Register(
	ctx context.Context, rq *api.RegisterRequest) (*api.RegisterResponse, error) {

	err := site.Register(rq)
	if err != nil {
		return nil, site.ErrorE("register failed", err)
	}

	return &api.RegisterResponse{}, nil
}

// TODO: Handle authorization, and how to handle reconnects of drivers
// ~~ Special token?
func (s *srv) Unregister(
	ctx context.Context, rq *api.UnregisterRequest) (*api.UnregisterResponse, error) {

	err := site.Unregister(rq)
	if err != nil {
		return nil, site.ErrorE("unregister failed", err)
	}

	return &api.UnregisterResponse{}, nil
}

func (s *srv) Status(ctx context.Context, rq *api.StatusRequest) (*api.StatusResponse, error) {

	return site.ForwardStatusRequest(rq)

}

func (s *srv) Setup(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpSetup, rq)

}

func (s *srv) Recycle(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpRecycle, rq)

}

func (s *srv) Configure(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpConfigure, rq)

}

func (s *srv) Reset(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpReset, rq)

}

func (s *srv) Restart(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpRestart, rq)

}

func (s *srv) TurnOn(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpOn, rq)

}

func (s *srv) TurnOff(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpOff, rq)

}

func (s *srv) Health(
	ctx context.Context, e *api.HealthRequest) (resp *api.HealthResponse, err error) {

	log.Info("health check")
	return site.Health(), nil

}

func (s *srv) NotifyIncoming(
	ctx context.Context, rq *api.IncomingRequest,
) (*api.IncomingResponse, error) {

	return &api.IncomingResponse{}, site.ForwardIncomingRequest(rq)

}

func (s *srv) CreateLink(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpCreate, rq)

}

func (s *srv) DestroyLink(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpDestroy, rq)

}

func (s *srv) ModifyLink(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpModify, rq)

}

func (s *srv) ConnectLink(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpConnect, rq)

}

func (s *srv) DisconnectLink(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {

	return site.ForwardMzRequest(site.OpDisconnect, rq)

}

// Assets
func (s *srv) CreateVolume(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {
	log.Infof("recvd: %v", rq)
	return site.ForwardMzRequest(site.OpAssetCreateVolume, rq)
}
func (s *srv) DestroyVolume(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {
	return site.ForwardMzRequest(site.OpAssetDestroyVolume, rq)
}
func (s *srv) ModifyVolume(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {
	return site.ForwardMzRequest(site.OpAssetModifyVolume, rq)
}
func (s *srv) CreateImage(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {
	return site.ForwardMzRequest(site.OpAssetCreateImage, rq)
}
func (s *srv) DestroyImage(ctx context.Context, rq *api.MzRequest) (*api.MzResponse, error) {
	return site.ForwardMzRequest(site.OpAssetDestroyImage, rq)
}

// Assets
func (s *srv) CreateSiteVolume(ctx context.Context, rq *api.AssetRequest) (*api.AssetResponse, error) {
	return site.ForwardAssetRequest(site.OpAssetCreateSiteVolume, rq)
}
func (s *srv) DestroySiteVolume(ctx context.Context, rq *api.AssetRequest) (*api.AssetResponse, error) {
	return site.ForwardAssetRequest(site.OpAssetDestroySiteVolume, rq)
}
func (s *srv) ModifySiteVolume(ctx context.Context, rq *api.AssetRequest) (*api.AssetResponse, error) {
	return site.ForwardAssetRequest(site.OpAssetModifySiteVolume, rq)
}

func main() {

	flag.Parse()
	site.InitLogging()

	log.Infof("commander %s", site.Version)

	creds, err := credentials.NewServerTLSFromFile(*Cert, *Key)
	if err != nil {
		log.WithError(err).Fatal("failed to load credentials")
	}
	GRPCServerOptions = append(GRPCServerOptions, grpc.Creds(creds))
	grpcServer := grpc.NewServer(GRPCServerOptions...)
	api.RegisterCommanderServer(grpcServer, &srv{})

	// this could probably be tcp, which should listen on tcp4 and tcp6
	// TODO: Add discovery mechanism for commander/driver
	addr := fmt.Sprintf("%s:%d", *CmdrHost, *CmdrPort)
	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("[Main] failed to listen: %v", err)
	}

	log.Infof("[Main] Listening on tcp://%s", addr)
	grpcServer.Serve(l)
}
