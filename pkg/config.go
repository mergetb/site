package site

import (
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var (
	// Version of the site api
	Version = "undefined"
)

// ElasticConfig host,port tuple
type ElasticConfig struct {
	Host string
	Port int
}

// Config is a wrapper for Elastic(Search)Config
type Config struct {
	//Optional - When present the commander will send logs to elasticsearch
	Elasticsearch *ElasticConfig
}

var c *Config

//GetConfig returns the global config (ElasticConfig)
func GetConfig() Config {
	if c == nil {
		LoadConfig()
	}
	return *c
}

//LoadConfig reads in merge.yml into the global config
func LoadConfig() {

	data, err := ioutil.ReadFile("/etc/merge/merge.yml")
	if err != nil {
		log.Fatalf("could not read configuration file /etc/merge/merge.yml %v", err)
	}

	cfg := &Config{}
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		log.Fatalf("could not parse configuration file %v", err)
	}

	c = cfg

}
