package site

import (
	api "gitlab.com/mergetb/site/api"
)

const (
	// DTrace Diagnostic Trace
	DTrace = api.Diagnostic_Trace
	// DDebug Diagnostic Debug
	DDebug = api.Diagnostic_Debug
	// DInfo Diagnostic Info
	DInfo = api.Diagnostic_Info
	// DWarn Diagnostic Warn
	DWarn = api.Diagnostic_Warn
	// DError Diagnostic Error
	DError = api.Diagnostic_Error
	// DCritical Diagnostic Critical
	DCritical = api.Diagnostic_Critical
)

// Dtrace produces a trace message with the provided message and fields
func Dtrace(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Trace,
		Msg:    message,
		Fields: fields,
	}
}

// Ddebug produces a debug message with the provided message and fields
func Ddebug(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Debug,
		Msg:    message,
		Fields: fields,
	}
}

// Dinfo produces an info message with the provided message and fields
func Dinfo(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Info,
		Msg:    message,
		Fields: fields,
	}
}

// Dwarning produces a warning message with the provided message and fields
func Dwarning(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Warn,
		Msg:    message,
		Fields: fields,
	}
}

// Derror produces an error message with the provided message and fields
func Derror(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Error,
		Msg:    message,
		Fields: fields,
	}
}

// Dcritical produces a critical message with the provided message and fields
func Dcritical(message string, fields map[string]string) *api.Diagnostic {
	return &api.Diagnostic{
		Code:   api.Diagnostic_Critical,
		Msg:    message,
		Fields: fields,
	}
}
