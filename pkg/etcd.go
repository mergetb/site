package site

import (
	"context"
	"encoding/json"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
)

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*                            ETCD   STORE                               */
/*                                                                       */
/*  /drivers/$NAME -> DriverInfo                                         */
/*  /devices/$NAME -> DeviceInfo                                         */
/*                                                                       */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

func etcdClient() (*etcd.Client, error) {
	log.WithFields(log.Fields{
		"host": *DbHost,
		"port": *DbPort,
	}).Trace("connecting to commander DB")
	return etcd.New(etcd.Config{Endpoints: []string{hostString(*DbHost, *DbPort)}})
}

func defaultContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(
		context.Background(),
		time.Duration(*EtcdTimeout)*time.Millisecond,
	)
}

// InsertDriver insert top level driver information
func InsertDriver(name, host string, port int) error {

	di := &DriverInfo{
		Name: name,
		Host: host,
		Port: port,
	}
	err := Write(di)
	if err != nil {
		return ErrorE("failed to insert driver", err)
	}

	return nil
}

// InsertDevice insert top level device information
// TODO authentication on who can insert where, two drivers named "A" shouldnt
// happen but how do we handle it if they do, how do we know which is the first
// to register and which is the second?
func InsertDevice(driver, id string) error {

	di := &DeviceInfo{
		ID:     id,
		Driver: driver,
	}
	err := Write(di)
	if err != nil {
		return ErrorE("failed to insert device", err)
	}

	return nil

}

// DeviceExists checks if a device exists given an id
func DeviceExists(id string) (bool, error) {

	di := &DeviceInfo{ID: id}
	err := ReadNew(di)
	if err != nil {
		return false, err
	}
	return di.GetVersion() > 0, nil

}

// RemoveDevices removes a set of devices from a site
// TODO: mechanism to check that the device is authorized to remove specified key
func RemoveDevices(devices []string) error {

	for _, x := range devices {

		di := &DeviceInfo{ID: x}
		err := Delete(di)
		if err != nil {
			return err
		}

	}

	return nil
}

// helpers ====================================================================

// will break if uuids are allowed to be non-unique globally (unique to device)

func getAllDrivers() ([]*DriverInfo, error) {

	cli, err := etcdClient()
	if err != nil {

		return nil, ErrorE("failed to connect to etcd", err)

	}
	defer cli.Close()

	resp, err := cli.Get(context.TODO(), driverPrefix, etcd.WithPrefix())
	if err != nil {
		return nil, ErrorE("failed to list drivers", err)
	}

	var result []*DriverInfo
	for _, kv := range resp.Kvs {
		di := &DriverInfo{}
		err = json.Unmarshal(kv.Value, di)
		if err != nil {
			log.WithError(err).Error("invalid driver info")
			continue
		}
		result = append(result, di)
	}

	return result, nil

}

// EtcdConnect Try up to 10 times to connect to etcd
func EtcdConnect() (*etcd.Client, error) {
	log.Trace("connecting to etcd...")
	cli, err := etcdClient()
	if err == nil {
		return cli, nil
	}
	for i := 0; i < 10; i++ {
		ErrorE("error connecting to etcd", err)
		time.Sleep(1 * time.Second)
		cli, err = etcdClient()
		if err != nil {
			return cli, nil
		}
	}
	return nil, ErrorE("failed to connect to etcd", err)
}

func withEtcd(f func(*etcd.Client) error) error {

	cli, err := EtcdConnect()
	if err != nil {
		return err
	}
	defer cli.Close()

	return f(cli)

}
