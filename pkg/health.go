package site

import (
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/site/api"
)

// Health checks and returns the health of the drivers
func Health() *api.HealthResponse {

	drivers, err := getAllDrivers()
	if err != nil {
		log.WithError(err).Error("failed to fetch drivers")
		return &api.HealthResponse{
			Drivers:        -1,
			HealthyDrivers: -1,
		}
	}

	result := &api.HealthResponse{}
	for _, x := range drivers {

		result.Drivers++
		err := driverHealthCheck(x.Host, x.Port)
		if err == nil {
			result.HealthyDrivers++
		}

	}
	return result

}
