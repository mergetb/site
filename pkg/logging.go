package site

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"runtime"
)

//InitLogging sets the initial log level to Debug
func InitLogging() {

	log.SetLevel(log.DebugLevel)
}

// Error Encapsulate err in a structured log and return an abstracted high-level
// error with message as the payload
func Error(message string) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).Error(message)

	return fmt.Errorf(message)

}

// ErrorE Encapsulate err in a structured log and return an abstracted high-level
// error with message as the payload
func ErrorE(message string, err error) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"error":  err,
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).Error(message)

	return fmt.Errorf(message)

}

// ErrorF Encapsulate fields in a structured log and return an abstracted high-level
// error with message as the payload
func ErrorF(message string, fields log.Fields) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).WithFields(fields).Error(message)

	return fmt.Errorf(message)

}

// ErrorEF Encapsulate fields and err in a structured log and return an abstracted
// high-level error with message as the payload
func ErrorEF(message string, err error, fields log.Fields) error {

	_, file, line, _ := runtime.Caller(1)

	log.WithFields(log.Fields{
		"error":  err,
		"caller": fmt.Sprintf("%s:%d", file, line),
	}).WithFields(fields).Error(message)

	return fmt.Errorf(message)

}
