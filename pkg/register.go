package site

import (
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/site/api"
)

// Register a set of devices with a driver to the commander
// TODO: before commander responds, it should do lookup to prevent reflected
// spoofs
func Register(rq *api.RegisterRequest) error {

	log.Trace("register")

	fields := log.Fields{
		"driver": rq.Driver,
		"host":   rq.Host,
	}

	// if contents are empty send back error
	if rq.Driver == "" || len(rq.Ids) < 1 {
		err := ErrorF("driver and devices annot be empty", fields)
		return err
	}

	// before we allow a driver to register with the commander,
	// let us first do a healthcheck to verify it is working as intended
	err := driverHealthCheck(rq.Host, int(rq.Port))
	if err != nil {
		return err
	}

	// write the update to the file for recovery if no errors occurred
	// TODO: Handle authorization, and how to handle reconnects of drivers
	// ~~ Special token?
	err = InsertDriver(rq.Driver, rq.Host, int(rq.Port))
	if err != nil {
		return ErrorEF("failed to add driver to datastore", err, fields)
	}

	// iterate over devices, find associated driver and add to datastore
	for _, id := range rq.Ids {

		fields["id"] = id

		exists, err := DeviceExists(id)
		if err != nil {
			return ErrorEF("failed to check for device", err, fields)
		}
		if exists {
			continue
		}

		log.WithFields(fields).Info("registering device")

		err = InsertDevice(rq.Driver, id)
		if err != nil {
			return ErrorE("insert driver error", err)
		}
	}

	return nil

}

// Unregister a set of devices (from driver) with commander
func Unregister(rq *api.UnregisterRequest) error {

	log.Trace("unregister")

	err := RemoveDevices(rq.Ids)
	if err != nil {
		return ErrorE("unregister failed", err)
	}

	return nil

}
