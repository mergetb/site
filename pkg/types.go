package site

import (
	"fmt"
)

// DriverInfo struct container connection info for a driver
type DriverInfo struct {
	Name string
	Host string
	Port int

	ver int64
}

// Key the etcd key used to prefix driver info
func (d *DriverInfo) Key() string {
	return fmt.Sprintf("/driver/%s", d.Name)
}

// GetVersion returns version from driver info
func (d *DriverInfo) GetVersion() int64 { return d.ver }

// SetVersion stors version to driver info
func (d *DriverInfo) SetVersion(v int64) { d.ver = v }

// Value returns the driver info struct
func (d *DriverInfo) Value() interface{} { return d }

// Endpoint returns the host:port string of the driver info
func (d *DriverInfo) Endpoint() string {
	return fmt.Sprintf("%s:%d", d.Host, d.Port)
}

// DeviceInfo stores the driver and id of a device
type DeviceInfo struct {
	ID     string
	Driver string

	ver int64
}

// Key the etcd key used to prefix device info
func (d *DeviceInfo) Key() string {
	return fmt.Sprintf("/device/%s", d.ID)
}

// GetVersion returns version from device info
func (d *DeviceInfo) GetVersion() int64 { return d.ver }

// SetVersion stors version to device info
func (d *DeviceInfo) SetVersion(v int64) { d.ver = v }

// Value returns the device info struct
func (d *DeviceInfo) Value() interface{} { return d }
