package site

import (
	"context"
	"encoding/binary"
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	api "gitlab.com/mergetb/site/api"
)

// Op is the operation
type Op int

const (
	OpIncoming               Op = 0
	OpStatus                    = 1
	OpSetup                     = 2
	OpRecycle                   = 3
	OpConfigure                 = 4
	OpReset                     = 5
	OpRestart                   = 6
	OpOn                        = 7
	OpOff                       = 8
	OpCreate                    = 9
	OpDestroy                   = 10
	OpModify                    = 11
	OpConnect                   = 12
	OpDisconnect                = 13
	OpAssetCreateVolume         = 14
	OpAssetDestroyVolume        = 15
	OpAssetModifyVolume         = 16
	OpAssetCreateImage          = 17
	OpAssetDestroyImage         = 18
	OpAssetCreateSiteVolume     = 19
	OpAssetDestroySiteVolume    = 20
	OpAssetModifySiteVolume     = 21
)

func (op Op) String() string {

	switch op {
	case OpIncoming:
		return "incoming"

	case OpSetup:
		return "setupNode"
	case OpRecycle:
		return "recycleNode"
	case OpConfigure:
		return "configureNode"
	case OpReset:
		return "resetNode"
	case OpOn:
		return "onNode"
	case OpOff:
		return "offNode"

	case OpCreate:
		return "createLink"
	case OpDestroy:
		return "destroyLink"
	case OpConnect:
		return "connectLink"
	case OpDisconnect:
		return "disconnectLink"

	case OpAssetCreateVolume:
		return "createVolume"
	case OpAssetDestroyVolume:
		return "destroyVolume"
	case OpAssetModifyVolume:
		return "modifyVolume"
	case OpAssetCreateImage:
		return "createImage"
	case OpAssetDestroyImage:
		return "destroyImage"

	case OpAssetCreateSiteVolume:
		return "createSiteVolume"
	case OpAssetDestroySiteVolume:
		return "destroySiteVolume"
	case OpAssetModifySiteVolume:
		return "modifySiteVolume"

	default:
		return "unknown op"
	}

}

// ForwardStatusRequest forwards request to drivers
func ForwardStatusRequest(rq *api.StatusRequest) (*api.StatusResponse, error) {

	ds, err := getAllDrivers()
	if err != nil {
		return nil, ErrorE("driver query failed", err)
	}

	var tasks []*api.Task

	for _, d := range ds {

		err := withDriver(d.Endpoint(), func(cli api.DriverClient, ctx context.Context) error {

			fields := log.Fields{
				"driver": d.Endpoint(),
			}
			log.WithFields(fields).Info("sending status request to driver")

			s, err := cli.Status(ctx, rq)
			if err != nil {
				return ErrorEF("driver rejected status request", err, log.Fields{
					"endpoint": d.Endpoint(),
				})
			}
			tasks = append(tasks, s.Tasks...)
			return nil

		})

		if err != nil {
			return nil, err
		}
	}

	return &api.StatusResponse{Tasks: tasks}, nil

}

// ForwardIncomingRequest to drivers
func ForwardIncomingRequest(rq *api.IncomingRequest) error {

	ds, err := getAllDrivers()
	if err != nil {
		return ErrorE("driver query failed", err)
	}

	for _, d := range ds {

		err := withDriver(d.Endpoint(), func(cli api.DriverClient, ctx context.Context) error {

			fields := log.Fields{
				"driver": d.Endpoint(),
				"code":   api.IncomingRequest_Code_name[int32(rq.Code)],
			}
			log.WithFields(fields).Info("sending incoming to driver")

			_, err := cli.NotifyIncoming(ctx, rq)
			if err != nil {
				return ErrorEF("driver rejected incoming", err, log.Fields{
					"endpoint": d.Endpoint(),
				})
			}
			return nil

		})

		if err != nil {
			return err
		}
	}

	return nil

}

// ForwardMzRequest to drivers
func ForwardMzRequest(op Op, rq *api.MzRequest) (*api.MzResponse, error) {

	log.Trace(op.String())

	log.Infof("%v", rq)

	// associate each request to a driver
	dmap, err := mapRequestToDrivers(rq.Fragments)
	if err != nil {
		return nil, err
	}

	// send the requests to the associated driver
	var responses []*api.MzResponse
	for driver, deviceInfos := range dmap {
		resp, err := sendMzRequestToDriver(rq.Mzid, rq.Instance, driver, op, deviceInfos)
		if err != nil {
			return nil, err
		}
		responses = append(responses, resp)
	}

	return mergeDriverResponses(responses), nil

}

// ForwardAssetRequest to drivers responsible for handling request
func ForwardAssetRequest(op Op, rq *api.AssetRequest) (*api.AssetResponse, error) {
	fields := log.Fields{
		"operation": op.String(),
		"request":   rq,
	}
	log.WithFields(fields).Infof("forwarding to driver")

	// associate each request to a driver
	dmap, err := mapAssetRequestToDrivers(rq.Fragments)
	if err != nil {
		return nil, err
	}

	// send the requests to the associated driver
	var responses []*api.AssetResponse
	for driver, deviceInfos := range dmap {
		resp, err := sendAssetRequestToDriver(rq.Assetid, driver, op, deviceInfos)
		if err != nil {
			return nil, err
		}
		responses = append(responses, resp)
	}

	return driverAssetResponses(responses), nil
}

func driverAssetResponses(rs []*api.AssetResponse) *api.AssetResponse {
	result := &api.AssetResponse{}
	for _, x := range rs {
		result.Msg = append(result.Msg, x.Msg...)
		result.Updates = append(result.Updates, x.Updates...)
	}

	return result
}

// for each asset fragment request, find the associated resources, then check the resource map
// to find which driver is responsible for that resource
func mapAssetRequestToDrivers(fragments []*api.AssetFragment) (
	map[string][]*api.AssetFragment, error,
) {
	// collect device info
	dimap := make(map[string]*DeviceInfo)
	var devinfos []Object
	for _, x := range fragments {
		for _, r := range x.Resources {
			di := &DeviceInfo{ID: r}
			devinfos = append(devinfos, di)
			dimap[r] = di
		}
	}
	// read all the devices in one shot
	_, err := ReadObjects(devinfos)
	if err != nil {
		return nil, ErrorE("failed to read device objects", err)
	}

	// create the mapping
	dmap := make(map[string][]*api.AssetFragment)
	for _, x := range fragments {
		for _, r := range x.Resources {
			driver := dimap[r].Driver
			if driver == "" {
				log.WithFields(log.Fields{"resource": r}).Warn("resource has no driver")
				continue
			}
			add := true
			for _, y := range dmap[driver] {
				if y == x {
					add = false
				}
			}
			if add {
				dmap[driver] = append(dmap[driver], x)
			}
		}
	}

	return dmap, nil
}

func sendAssetRequestToDriver(
	assetid, driver string, op Op, fragments []*api.AssetFragment,
) (*api.AssetResponse, error) {

	fields := log.Fields{"driver": driver, "op": op.String()}
	log.WithFields(fields).Info("sending request to driver")

	di := &DriverInfo{Name: driver}
	err := Read(di)
	if err != nil {
		return nil, ErrorEF("failed to get driver", err, fields)
	}

	// set up a driver response handler
	handleAssetResponse := func(resp *api.AssetResponse, err error) (*api.AssetResponse, error) {
		if err != nil {
			return resp, ErrorEF("request failed", err, fields)
		}
		return resp, nil
	}

	rq := &api.AssetRequest{
		Assetid:   assetid,
		Fragments: fragments,
	}

	var resp *api.AssetResponse
	err = withDriver(di.Endpoint(), func(cli api.DriverClient, ctx context.Context) error {

		switch op {
		case OpAssetCreateSiteVolume:
			resp, err = handleAssetResponse(cli.CreateSiteVolume(ctx, rq))
		case OpAssetDestroySiteVolume:
			resp, err = handleAssetResponse(cli.DestroySiteVolume(ctx, rq))
		case OpAssetModifySiteVolume:
			resp, err = handleAssetResponse(cli.ModifySiteVolume(ctx, rq))
		default:
			err = fmt.Errorf("unknown op")
			log.WithFields(fields).Warning("bad op received")
		}

		return err
	})

	return resp, err
}

// helpers ====================================================================

func sendMzRequestToDriver(
	mzid, instance, driver string, op Op, fragments []*api.MzFragment,
) (*api.MzResponse, error) {

	fields := log.Fields{"driver": driver, "op": op.String()}
	log.WithFields(fields).Info("sending request to driver")

	di := &DriverInfo{Name: driver}
	err := Read(di)
	if err != nil {
		return nil, ErrorEF("failed to get driver", err, fields)
	}

	// set up a driver response handler
	handleMzResponse := func(resp *api.MzResponse, err error) (*api.MzResponse, error) {
		if err != nil {
			return resp, ErrorEF("request failed", err, fields)
		}
		return resp, nil
	}

	rq := &api.MzRequest{
		Mzid:      mzid,
		Instance:  instance,
		Fragments: fragments,
	}

	var resp *api.MzResponse
	err = withDriver(di.Endpoint(), func(cli api.DriverClient, ctx context.Context) error {

		switch op {

		// node ops
		case OpOn:
			resp, err = handleMzResponse(cli.TurnOn(ctx, rq))

		case OpOff:
			resp, err = handleMzResponse(cli.TurnOff(ctx, rq))

		case OpRestart:
			resp, err = handleMzResponse(cli.Restart(ctx, rq))

		case OpReset:
			resp, err = handleMzResponse(cli.Reset(ctx, rq))

		case OpSetup:
			resp, err = handleMzResponse(cli.Setup(ctx, rq))

		case OpRecycle:
			resp, err = handleMzResponse(cli.Recycle(ctx, rq))

		case OpConfigure:
			resp, err = handleMzResponse(cli.Configure(ctx, rq))

		// link ops
		case OpCreate:
			resp, err = handleMzResponse(cli.CreateLink(ctx, rq))

		case OpDestroy:
			resp, err = handleMzResponse(cli.DestroyLink(ctx, rq))

		case OpModify:
			resp, err = handleMzResponse(cli.ModifyLink(ctx, rq))

		case OpConnect:
			resp, err = handleMzResponse(cli.ConnectLink(ctx, rq))

		case OpDisconnect:
			resp, err = handleMzResponse(cli.DisconnectLink(ctx, rq))

		// asset ops
		case OpAssetCreateVolume:
			resp, err = handleMzResponse(cli.CreateVolume(ctx, rq))

		case OpAssetDestroyVolume:
			resp, err = handleMzResponse(cli.DestroyVolume(ctx, rq))

		case OpAssetModifyVolume:
			resp, err = handleMzResponse(cli.ModifyVolume(ctx, rq))

		case OpAssetCreateImage:
			resp, err = handleMzResponse(cli.CreateImage(ctx, rq))

		case OpAssetDestroyImage:
			resp, err = handleMzResponse(cli.DestroyImage(ctx, rq))

		default:
			err = fmt.Errorf("unknown op")
			log.WithFields(fields).Warning("bad op received")

		}

		return err

	})

	return resp, err

}

func getDriverHost(name string) (string, error) {

	di := &DriverInfo{Name: name}
	err := Read(di)
	if err != nil {
		return "", err
	}
	return di.Host, nil

}

func mergeDriverResponses(rs []*api.MzResponse) *api.MzResponse {

	result := &api.MzResponse{}

	for _, x := range rs {
		result.Results = append(result.Results, x.Results...)
	}

	return result

}

// ErrToDiag add error message to diagnostics
func ErrToDiag(err error) *api.Diagnostic {

	return NewDiag(1, err.Error())

}

func mapRequestToDrivers(fragments []*api.MzFragment) (map[string][]*api.MzFragment, error) {

	// collect device info
	dimap := make(map[string]*DeviceInfo)
	var devinfos []Object
	for _, x := range fragments {
		for _, r := range x.Resources {
			di := &DeviceInfo{ID: r}
			devinfos = append(devinfos, di)
			dimap[r] = di
		}
	}
	// read all the devices in one shot
	_, err := ReadObjects(devinfos)
	if err != nil {
		return nil, ErrorE("failed to read device objects", err)
	}

	// create the mapping
	dmap := make(map[string][]*api.MzFragment)
	for _, x := range fragments {
		for _, r := range x.Resources {

			driver := dimap[r].Driver
			if driver == "" {
				log.WithFields(log.Fields{"resource": r}).Warn("resource has no driver")
				continue
			}
			add := true
			for _, y := range dmap[driver] {
				if y == x {
					add = false
				}
			}
			if add {
				dmap[driver] = append(dmap[driver], x)
			}

		}
	}

	return dmap, nil

}

func driverHealthCheck(host string, port int) error {

	portStr := strconv.Itoa(port)
	HostPort := host + ":" + portStr

	// phone the client and ask if it is up and running
	conn, err := grpc.Dial(HostPort, grpc.WithInsecure())
	if err != nil {
		return ErrorE("failed to dial back to driver server", err)
	}
	defer conn.Close()

	// try connecting - verify that service is running.
	client := api.NewDriverClient(conn)

	// create a context
	ctx, cancel := context.WithTimeout(context.Background(), 80*time.Millisecond)
	defer cancel()

	req := &api.HealthRequest{}
	_, err = client.Health(ctx, req)
	if err != nil {
		return ErrorE("device driver failed health check", err)
	}
	return nil

}

// convert host, port string, into a usable string to connect to
func hostString(host string, port int) string {
	portStr := strconv.Itoa(int(port))
	hostStr := host + ":" + portStr
	return hostStr
}

func toUInt64(v []byte) int {
	x, _ := binary.Uvarint(v)
	return int(x)
}

// NewDiag creates a new Diagnostic struct
func NewDiag(code int, format string, args ...interface{}) *api.Diagnostic {
	return &api.Diagnostic{
		Code: api.Diagnostic_Code(code),
		Msg:  fmt.Sprintf(format, args...),
	}
}

func byteToStr(input []byte) string {
	return string(input[:len(input)])
}

func withDriver(endpoint string, f func(api.DriverClient, context.Context) error) error {

	fields := log.Fields{"endpoint": endpoint}

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		return ErrorEF("failed to dial back to driver server", err, fields)
	}
	defer conn.Close()
	client := api.NewDriverClient(conn)
	log.WithFields(fields).Trace("connected to driver")

	// setup calling context
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(*DriverTimeout)*time.Second,
	)
	defer cancel()

	return f(client, ctx)

}
