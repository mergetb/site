package site

import "flag"

var (
	DbPort        = flag.Int("dbport", 2399, "etcd port")
	DbHost        = flag.String("dbhost", "localhost", "etcd host")
	EtcdTimeout   = flag.Int("etcd-timeout", 100, "request timeout (ms)")
	DriverTimeout = flag.Int("driver-timeout", 100, "request timeout (s)")
)

var (
	driverPrefix = "/driver/"
	driverHost   = "/host"
	driverPort   = "/port"
	devicePrefix = "/devices/"
)
