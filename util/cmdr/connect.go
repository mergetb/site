package main

import (
	"context"
	"log"
	"time"

	api "gitlab.com/mergetb/site/api"

	"google.golang.org/grpc"
)

func withCommander(f func(api.CommanderClient, context.Context)) {

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to dial commander: %v", err)
	}
	defer conn.Close()
	client := api.NewCommanderClient(conn)

	// setup calling context
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(timeout)*time.Second,
	)
	defer cancel()

	f(client, ctx)

}
