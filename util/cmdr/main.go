package main

import (
	"context"
	"log"
	"os"

	"github.com/alecthomas/chroma/quick"
	"github.com/mergetb/yaml"
	"github.com/spf13/cobra"
	api "gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/site/pkg"
)

var (
	endpoint string
	timeout  int
)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "cmdr",
		Short: "Interact with site commanders",
	}
	root.PersistentFlags().StringVarP(
		&endpoint, "endpoint", "e", "localhost:6000",
		"endpoint to reach commander at",
	)
	root.PersistentFlags().IntVarP(
		&timeout, "timeout", "t", 3,
		"commander connection timeout",
	)

	version := &cobra.Command{
		Use:   "version",
		Short: "show version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Print(site.Version)
		},
	}
	root.AddCommand(version)

	health := &cobra.Command{
		Use:   "health",
		Short: "Get commander health",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			health()
		},
	}
	root.AddCommand(health)

	root.Execute()

}

func health() {

	withCommander(func(c api.CommanderClient, ctx context.Context) {

		health, err := c.Health(ctx, &api.HealthRequest{})
		if err != nil {
			log.Fatal(err)
		}

		out, err := yaml.Marshal(health)
		if err != nil {
			log.Fatal(err)
		}

		quick.Highlight(os.Stdout, string(out), "yaml", "terminal256", "pygments")

	})

}
